package com.testapp.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class CurrenciesHelper {
    private static DecimalFormat decimalFormat = new DecimalFormat("#.####", new DecimalFormatSymbols(Locale.US));

    public static String getCurrencySymbol(String currency) {
        switch (currency.toLowerCase()) {
            case "usd":
                return "$";
            case "eur":
                return "€";
            case "gbp":
                return "£";
        }

        return "";
    }

    public static String getExhangeValueString(String baseCurrency, String targetCurrency, double rateValue) {
        return CurrenciesHelper.getCurrencySymbol(baseCurrency) + "1 = " +
                CurrenciesHelper.getCurrencySymbol(targetCurrency) +
                decimalFormat.format(rateValue);
    }
}
