package com.testapp.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class CurrenciesPagerAdapter extends FragmentStatePagerAdapter {
    private static final int SIZE = 1000;

    private List<String> currencies = new ArrayList<>();
    private boolean base;
    private SparseArray<WeakReference<CurrencyPageFragment>> registeredFragments = new SparseArray<>();

    public CurrenciesPagerAdapter(FragmentManager fragmentManager, List<String> currencies, boolean base) {
        super(fragmentManager);

        this.currencies.addAll(currencies);
        this.base = base;
    }

    @Override
    public int getCount() {
        if (currencies.size() < 2) {
            return currencies.size();
        }
        return SIZE;
    }

    public int getInMiddleFirstIndex() {
        if (getCount() < 2) {
            return 0;
        }

        int i = SIZE / 2;
        int c = currencies.size();

        return i - i % c;
    }

    public int getRealIndex(int index) {
        return index % currencies.size();
    }

    @Override
    public Fragment getItem(int position) {
        return CurrencyPageFragment.create(getCurrency(position), base);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        registeredFragments.put(position, new WeakReference<>((CurrencyPageFragment) object));
        return object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);

        registeredFragments.remove(position);
    }

    public String getCurrency(int position) {
        return currencies.get(getRealIndex(position));
    }

    public int getPosition(String currency, int currentPosition) {
        int itemIndex = currencies.indexOf(currency);
        return currentPosition + itemIndex % currencies.size();
    }

    public CurrencyPageFragment getInstatiatedFragment(int position) {
        WeakReference<CurrencyPageFragment> reference = registeredFragments.get(position);
        if (reference == null) {
            return null;
        }

        return reference.get();
    }
}
