package com.testapp.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.testapp.App;
import com.testapp.Constants;
import com.testapp.R;
import com.testapp.databinding.ActivityExchangeBinding;
import com.testapp.events.CurrencyValueChanged;
import com.testapp.mvp.model.RatesViewModel;
import com.testapp.mvp.presenter.ExchangePresenter;
import com.testapp.mvp.view.ExchangeView;
import com.testapp.ui.view.DotsIndicatorView;
import com.testapp.utils.CurrenciesHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ExchangeActivity extends BaseActivity<ExchangeView, ExchangePresenter> implements ExchangeView {
    private ActivityExchangeBinding binding;
    private Snackbar loadingSnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_exchange);

        binding.ratesTitle.setVisibility(View.INVISIBLE);

        initViewPager(false);
        initViewPager(true);
    }

    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);
    }

    @NonNull
    @Override
    public ExchangePresenter createPresenter() {
        return App.getAppComponent().exchangePresenter();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initViewPager(boolean base) {
        DotsIndicatorView dotsIndicatorView = base ? binding.topDotsIndicatorView : binding.bottomDotsIndicatorView;
        ViewPager viewPager = base ? binding.topViewPager : binding.bottomViewPager;
        String currency = base ? presenter.getBaseCurrency() : presenter.getTargetCurrency();

        dotsIndicatorView.setDotsCount(presenter.getCurrencies().size());

        CurrenciesPagerAdapter adapter = new CurrenciesPagerAdapter(getSupportFragmentManager(),
                presenter.getCurrencies(), base);
        viewPager.setAdapter(adapter);

        int position = adapter.getPosition(currency, adapter.getInMiddleFirstIndex());
        viewPager.setCurrentItem(position);
        dotsIndicatorView.setSelectedDot(adapter.getRealIndex(position), false);

        viewPager.addOnPageChangeListener(new OnPageChangeListenerSimple() {
            @Override
            public void onPageSelected(int position) {
                if (base) {
                    presenter.setBaseCurrency(adapter.getCurrency(position));
                } else {
                    presenter.setTargetCurrency(adapter.getCurrency(position));
                }
                dotsIndicatorView.setSelectedDot(adapter.getRealIndex(position), true);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CurrencyValueChanged event) {
        if (event.base) {
            presenter.setBaseValue(event.value);
        } else {
            presenter.setTargetValue(event.value);
        }
    }

    @Override
    public void showLoading() {
        loadingSnackbar = Snackbar.make(binding.getRoot(), "Rates are loading", Snackbar.LENGTH_INDEFINITE);
        loadingSnackbar.show();
    }

    @Override
    public void hideLoading() {
        if (loadingSnackbar != null) {
            loadingSnackbar.dismiss();
            loadingSnackbar = null;
        }
    }

    @Override
    public void showError(int errorCode) {
        String message;
        switch (errorCode) {
            case Constants.ERROR_CODE_NO_INTERNET_CONNECTION:
                message = "Check internet connection";
                break;
            case Constants.ERROR_CODE_REQUEST_ERROR:
                message = "Request data error";
                break;
            case Constants.ERROR_CODE_INTERNAL:
            default:
                message = "An error has occurred";
                break;
        }

        Snackbar errorSnackbar = Snackbar.make(binding.getRoot(), message, Snackbar.LENGTH_INDEFINITE);
        errorSnackbar.setAction(R.string.try_again_action, v -> presenter.onErrorTryAgain());
        errorSnackbar.show();
    }

    @Override
    public void renderData(RatesViewModel data) {
        binding.ratesTitle.setVisibility(data.rateValue > 0.0 ? View.VISIBLE : View.INVISIBLE);
        if (data.rateValue > 0.0) {
            binding.ratesTitle.setText(CurrenciesHelper.getExhangeValueString(presenter.getBaseCurrency(),
                    presenter.getTargetCurrency(), data.rateValue));
        }

        CurrencyPageFragment baseCurrencyFragment = ((CurrenciesPagerAdapter) binding.topViewPager.getAdapter()).
                getInstatiatedFragment(binding.topViewPager.getCurrentItem());
        CurrencyPageFragment targetCurrencyFragment = ((CurrenciesPagerAdapter) binding.bottomViewPager.getAdapter()).
                getInstatiatedFragment(binding.bottomViewPager.getCurrentItem());

        if (baseCurrencyFragment != null && targetCurrencyFragment != null) {
            baseCurrencyFragment.renderData(data);
            targetCurrencyFragment.renderData(data);
        }
    }

    private static abstract class OnPageChangeListenerSimple implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
