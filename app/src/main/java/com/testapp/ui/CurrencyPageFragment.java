package com.testapp.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.testapp.R;
import com.testapp.databinding.FragmentCurrencyPageBinding;
import com.testapp.events.CurrencyValueChanged;
import com.testapp.mvp.model.RatesViewModel;
import com.testapp.utils.CurrenciesHelper;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class CurrencyPageFragment extends Fragment {
    private final static String CHANGED_PROGRAMMATICALLY = "CHANGED_PROGRAMMATICALLY";

    public static CurrencyPageFragment create(String currency, boolean base) {
        CurrencyPageFragment currencyPageFragment = new CurrencyPageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("currency", currency);
        bundle.putBoolean("base", base);
        currencyPageFragment.setArguments(bundle);

        return currencyPageFragment;
    }

    private FragmentCurrencyPageBinding binding;
    private DecimalFormat decimalFormat = new DecimalFormat("#.####", new DecimalFormatSymbols(Locale.US));
    private boolean base;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_currency_page, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        base = getArguments().getBoolean("base");
        binding.title.setText(getArguments().getString("currency"));

        //TODO need to limit the number of digits that user can type
        RxTextView.textChanges(binding.editText)
                .subscribe(s -> {
                    if (s.length() > 10) {
                        binding.editText.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                getResources().getDimensionPixelSize(R.dimen.currency_value_size_xsmall));
                    } else if (s.length() > 5) {
                        binding.editText.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                getResources().getDimensionPixelSize(R.dimen.currency_value_size_small));
                    } else {
                        binding.editText.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                getResources().getDimensionPixelSize(R.dimen.currency_value_size_normal));
                    }

                    String text = s.toString();
                    if (!CHANGED_PROGRAMMATICALLY.equals(binding.editText.getTag()) &&
                            !TextUtils.isEmpty(text)) {
                        double value = 0.0;
                        try {
                            value = Double.valueOf(text);
                        } catch (NumberFormatException e) {//in a real project it's a wrong decision to check wrong user typing, bestway - implement own InputFilter
                            setValue(value);
                        }
                        EventBus.getDefault().post(new CurrencyValueChanged(Math.abs(value), base));
                    }
                });
    }

    private void setValue(double value) {
        binding.editText.setTag(CHANGED_PROGRAMMATICALLY);
        {
            String text = String.valueOf(value);
            int selectionOffset = binding.editText.getText().toString().length() - binding.editText.getSelectionEnd();
            binding.editText.setText(text);
            binding.editText.setSelection(text.length() - selectionOffset);
        }
        binding.editText.setTag(null);
    }

    public void renderData(RatesViewModel data) {
        binding.editText.setTag(CHANGED_PROGRAMMATICALLY);
        {
            double value = base ? data.baseValue : data.targetValue;

            String text = decimalFormat.format(value);
            if (value > 0.0) {
                text = (base ? "-" : "+") + text;
            }

            int selectionOffset = binding.editText.getText().toString().length() - binding.editText.getSelectionEnd();
            binding.editText.setText(text);
            binding.editText.setSelection(text.length() - selectionOffset);

            double rate = base ? data.rateValue : data.inverseRateValue;
            if (rate == 0.0) {
                binding.rate.setVisibility(View.INVISIBLE);
            } else {
                binding.rate.setVisibility(View.VISIBLE);
                if (base) {
                    binding.rate.setText(CurrenciesHelper.getExhangeValueString(data.baseCurrency,
                            data.targetCurrency, rate));
                } else {
                    binding.rate.setText(CurrenciesHelper.getExhangeValueString(data.targetCurrency,
                            data.baseCurrency, rate));
                }
            }
        }
        binding.editText.setTag(null);
    }
}
