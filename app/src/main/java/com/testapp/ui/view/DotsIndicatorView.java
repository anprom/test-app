package com.testapp.ui.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.testapp.R;

import java.util.ArrayList;
import java.util.List;

public class DotsIndicatorView extends View {
    private Paint paint;
    private int gap;
    private int activeRadius;
    private int inactiveRadius;
    private List<Dot> dots = new ArrayList<>();

    public DotsIndicatorView(@NonNull Context context) {
        super(context);

        init(null);
    }

    public DotsIndicatorView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public DotsIndicatorView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    public void init(AttributeSet attrs) {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.DotsIndicatorView);
        gap = typedArray.getDimensionPixelSize(R.styleable.DotsIndicatorView_div_gap, 0);
        activeRadius = typedArray.getDimensionPixelSize(R.styleable.DotsIndicatorView_div_activeRadius, 0);
        inactiveRadius = typedArray.getDimensionPixelSize(R.styleable.DotsIndicatorView_div_inactiveRadius, 0);
        typedArray.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (int i = 0; i < dots.size(); i++) {
            Dot dot = dots.get(i);
            paint.setAlpha(dot.alpha);
            canvas.drawCircle(getWidth() * 0.5f + (inactiveRadius * 2.f + gap) * (i - (dots.size() - 1) * 0.5f),
                    getHeight() * 0.5f, dot.radius, paint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(activeRadius * 2, MeasureSpec.EXACTLY));
    }

    public void setDotsCount(int dotsCount) {
        if (dots.size() != dotsCount) {
            while (dots.size() != dotsCount) {
                if (dots.size() < dotsCount) {
                    dots.add(new Dot(false));
                } else {
                    dots.remove(0);
                }
            }

            invalidate();
        }
    }

    public void setSelectedDot(int selectedDot, boolean animated) {
        if (selectedDot < 0 || selectedDot >= dots.size()) {
            throw new IllegalStateException("Wrong dot index: " + selectedDot);
        }

        for (int i = 0; i < dots.size(); i++) {
            Dot dot = dots.get(i);
            dot.setActive(i == selectedDot, animated);
        }

        invalidate();
    }

    private class Dot {
        boolean active;
        int alpha = 255;
        int radius;

        ValueAnimator scaleAnimator;
        ValueAnimator alphaAnimator;

        public Dot(boolean active) {
            setActive(active, false);
        }

        public void setActive(boolean active, boolean animated) {
            if (!animated) {
                radius = active ? activeRadius : inactiveRadius;
                alpha = active ? 255 : 155;
            } else if (this.active != active) {
                if (scaleAnimator != null && scaleAnimator.isRunning()) {
                    scaleAnimator.end();
                }
                if (alphaAnimator != null && alphaAnimator.isRunning()) {
                    alphaAnimator.end();
                }

                scaleAnimator = ValueAnimator.ofInt(active ? inactiveRadius : activeRadius,
                        !active ? inactiveRadius : activeRadius);
                scaleAnimator.setDuration(250);
                scaleAnimator.setInterpolator(new DecelerateInterpolator());
                scaleAnimator.addUpdateListener(animation -> {
                    radius = (int) animation.getAnimatedValue();
                    invalidate();
                });
                scaleAnimator.start();

                alphaAnimator = ValueAnimator.ofInt(active ? 155 : 255,
                        !active ? 155 : 255);
                alphaAnimator.setDuration(250);
                alphaAnimator.setInterpolator(new DecelerateInterpolator());
                alphaAnimator.addUpdateListener(animation -> {
                    alpha = (int) animation.getAnimatedValue();
                    invalidate();
                });
                alphaAnimator.start();
            }


            this.active = active;
        }
    }
}
