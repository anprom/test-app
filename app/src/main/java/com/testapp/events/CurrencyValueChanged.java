package com.testapp.events;

public class CurrencyValueChanged {
    public final double value;
    public final boolean base;

    public CurrencyValueChanged(double value, boolean base) {
        this.value = value;
        this.base = base;
    }
}
