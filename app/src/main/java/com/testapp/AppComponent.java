package com.testapp;

import com.testapp.model.FixerDataRepository;
import com.testapp.mvp.presenter.ExchangePresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    FixerDataRepository fixerDataRepository();
    ExchangePresenter exchangePresenter();
}