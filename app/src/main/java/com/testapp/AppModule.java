package com.testapp;

import com.testapp.model.FixerDataInteractor;
import com.testapp.model.FixerDataRepository;
import com.testapp.model.FixerRetrofitService;
import com.testapp.mvp.presenter.ExchangePresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private final FixerDataRepository fixerDataRepository;

    public AppModule(String baseUrl) {
        FixerRetrofitService fixerRetrofitService = new FixerRetrofitService(baseUrl);
        fixerDataRepository = new FixerDataRepository(fixerRetrofitService);
    }

    @Provides
    @Singleton
    FixerDataRepository provideFixerDataRepository() {
        return fixerDataRepository;
    }

    @Provides
    @Singleton
    ExchangePresenter provideExchangePresenter() {
        return new ExchangePresenter(new FixerDataInteractor());
    }
}
