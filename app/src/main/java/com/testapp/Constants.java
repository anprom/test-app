package com.testapp;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public final static List<String> CURRENCIES = Arrays.asList("USD", "EUR", "GBP");

    public final static int SYNC_INTERVAL_MS = 30_000;

    public final static int ERROR_CODE_NONE = 0;
    public final static int ERROR_CODE_NO_INTERNET_CONNECTION = 1;
    public final static int ERROR_CODE_REQUEST_ERROR = 2;
    public final static int ERROR_CODE_INTERNAL = 3;
}
