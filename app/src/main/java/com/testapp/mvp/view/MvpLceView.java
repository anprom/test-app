package com.testapp.mvp.view;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface MvpLceView<M> extends MvpView {
    void showLoading();

    void hideLoading();

    void showError(int errorCode);

    void renderData(M data);
}
