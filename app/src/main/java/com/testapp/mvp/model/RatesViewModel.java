package com.testapp.mvp.model;

public class RatesViewModel {
    public final double rateValue;
    public final double inverseRateValue;
    public final String baseCurrency;
    public final String targetCurrency;
    public final double baseValue;
    public final double targetValue;

    public RatesViewModel(double rateValue, double inverseRateValue, String baseCurrency, String targetCurrency, double baseValue, double targetValue) {
        this.rateValue = rateValue;
        this.inverseRateValue = inverseRateValue;
        this.baseCurrency = baseCurrency;
        this.targetCurrency = targetCurrency;
        this.baseValue = baseValue;
        this.targetValue = targetValue;
    }
}
