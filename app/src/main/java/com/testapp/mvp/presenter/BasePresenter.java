package com.testapp.mvp.presenter;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by eugene on 02.07.17.
 */

public abstract class BasePresenter<V extends MvpView> extends MvpBasePresenter<V> {
    public abstract void onStart();

    public abstract void onStop();
}
