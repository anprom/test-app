package com.testapp.mvp.presenter;

import com.testapp.Constants;
import com.testapp.model.FixerDataInteractor;
import com.testapp.model.entities.RatesState;
import com.testapp.mvp.model.RatesViewModel;
import com.testapp.mvp.view.ExchangeView;

import java.util.List;

public class ExchangePresenter extends BasePresenter<ExchangeView> {
    private FixerDataInteractor fixerDataInteractor;
    private String baseCurrency = Constants.CURRENCIES.get(0);
    private String targetCurrency = Constants.CURRENCIES.get(1);
    private double baseValue = 0.0;

    public ExchangePresenter(FixerDataInteractor fixerDataInteractor) {
        this.fixerDataInteractor = fixerDataInteractor;
    }

    @Override
    public void onStart() {
        updateView();

        restartSync();
    }

    @Override
    public void onStop() {
        fixerDataInteractor.dispose();
    }

    public void onErrorTryAgain() {
        restartSync();
    }

    public List<String> getCurrencies() {
        return Constants.CURRENCIES;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
        baseValue = 0.0;

        updateView();
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;

        updateView();
    }

    public void setBaseValue(double baseValue) {
        this.baseValue = baseValue;

        updateView();
    }

    public void setTargetValue(double targetValue) {
        double inverseRate = fixerDataInteractor.getRates().getRate(baseCurrency, targetCurrency).getInverseRate();
        baseValue = targetValue * inverseRate;

        updateView();
    }

    private void restartSync() {
        fixerDataInteractor.restartSync(this::updateView);
    }

    private void updateView() {
        updateView(fixerDataInteractor.getRates());
    }

    private void updateView(RatesState ratesState) {
        if (isViewAttached()) {
            if (ratesState.errorCode != Constants.ERROR_CODE_NONE) {
                getView().showError(ratesState.errorCode);
            } else if (!ratesState.loaded) {
                getView().showLoading();
            } else {
                getView().hideLoading();
            }

            getView().renderData(createRatesViewModel(ratesState));
        }
    }

    public RatesViewModel createRatesViewModel() {
        return createRatesViewModel(fixerDataInteractor.getRates());
    }

    public RatesViewModel createRatesViewModel(RatesState ratesState) {
        RatesState.Rate rate = ratesState.getRate(baseCurrency, targetCurrency);

        double targetValue = baseValue * rate.rate;
        if (baseCurrency.equals(targetCurrency)) {
            targetValue = baseValue;
        }

        return new RatesViewModel(rate.rate, rate.getInverseRate(), baseCurrency, targetCurrency,
                baseValue, targetValue);
    }
}
