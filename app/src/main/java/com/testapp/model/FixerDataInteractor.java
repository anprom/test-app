package com.testapp.model;

import com.testapp.App;
import com.testapp.Constants;
import com.testapp.model.entities.RatesState;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class FixerDataInteractor {
    private CompositeDisposable compositeDisposable;

    public void restartSync(Consumer<RatesState> onNext) {
        dispose();
        compositeDisposable = new CompositeDisposable();

        Disposable disposable = Observable
                .interval(0, Constants.SYNC_INTERVAL_MS, TimeUnit.MILLISECONDS, Schedulers.io())
                .flatMap(new Function<Long, ObservableSource<RatesState>>() {
                    @Override
                    public ObservableSource<RatesState> apply(@NonNull Long aLong) throws Exception {
                        return App.getAppComponent().fixerDataRepository().loadRatesState();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ratesState -> {
                    onNext.accept(ratesState);

                    if (ratesState.errorCode != Constants.ERROR_CODE_NONE) {
                        dispose();
                    }
                });

        compositeDisposable.add(disposable);
    }

    public RatesState getRates() {
        return App.getAppComponent().fixerDataRepository().get();
    }

    public void dispose() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
