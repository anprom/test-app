package com.testapp.model;

import android.text.TextUtils;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.testapp.model.entities.FixerDataJson;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class FixerRetrofitService {
    private FixerServiceInterface service;

    public FixerRetrofitService(String baseUrl) {
        service = buildRetrofit(baseUrl).create(FixerServiceInterface.class);
    }

    private Retrofit buildRetrofit(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Observable<FixerDataJson> latestRates(String base, List<String> symbols) {
        return service.latest(base, TextUtils.join(",", symbols));
    }

    interface FixerServiceInterface {
        @GET("latest")
        Observable<FixerDataJson> latest(@Query("base") String base, @Query("symbols") String symbols);
    }
}
