package com.testapp.model.entities;

import java.util.Map;

public class FixerDataJson {
    public String base;
    public String date;
    public Map<String, Double> rates;
}
