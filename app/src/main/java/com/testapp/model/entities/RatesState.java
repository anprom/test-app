package com.testapp.model.entities;

import com.annimon.stream.Stream;
import com.testapp.Constants;

import java.util.ArrayList;
import java.util.List;

public class RatesState {
    private List<Rate> rates = new ArrayList<>();
    public final boolean loaded;
    public final int errorCode;

    public RatesState(boolean loaded, int errorCode) {
        this.loaded = loaded;
        this.errorCode = errorCode;
    }

    public void addRate(double rate, String baseCurrency, String targetCurrency) {
        rates.add(new Rate(rate, baseCurrency, targetCurrency));
    }

    public Rate getRate(String baseCurrency, String targetCurrency) {
        if (!loaded || errorCode != Constants.ERROR_CODE_NONE || baseCurrency.equals(targetCurrency)) {
            return new Rate(0.0, baseCurrency, targetCurrency);
        }

        Rate rate = Stream.of(rates).filter(value ->
                value.baseCurrency.equals(baseCurrency) &&
                        value.targetCurrency.equals(targetCurrency)).findFirst().orElse(null);

        return rate == null ? new Rate(0.0, baseCurrency, targetCurrency) : rate;
    }

    public static class Rate {
        public final double rate;
        public final String baseCurrency;
        public final String targetCurrency;

        public Rate(double rate, String baseCurrency, String targetCurrency) {
            this.rate = rate;
            this.baseCurrency = baseCurrency;
            this.targetCurrency = targetCurrency;
        }

        public double getInverseRate() {
            return rate == 0.0 ? 0.0 : 1.0 / rate;
        }
    }
}
