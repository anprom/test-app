package com.testapp.model;

import com.testapp.Constants;
import com.testapp.model.entities.FixerDataJson;
import com.testapp.model.entities.RatesState;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class FixerDataRepository {
    private final FixerRetrofitService fixerRetrofitService;

    //super simple in-memory cache
    private RatesState ratesState = new RatesState(false, Constants.ERROR_CODE_NONE);

    public FixerDataRepository(FixerRetrofitService fixerRetrofitService) {
        this.fixerRetrofitService = fixerRetrofitService;
    }

    public Observable<RatesState> loadRatesState() {
        List<Observable<FixerDataJson>> requests = new ArrayList<>();
        for (String currency : Constants.CURRENCIES) {
            List<String> symbols = new ArrayList<>(Constants.CURRENCIES);
            symbols.remove(currency);
            requests.add(fixerRetrofitService.latestRates(currency, symbols));
        }

        return Observable.concat(requests)
                .subscribeOn(Schedulers.io())
                .toList()
                .map(fixerDataJsons -> {
                    RatesState ratesState = new RatesState(true, Constants.ERROR_CODE_NONE);
                    for (FixerDataJson fixerDataJson : fixerDataJsons) {
                        for (String targetCurrency : fixerDataJson.rates.keySet()) {
                            ratesState.addRate(fixerDataJson.rates.get(targetCurrency), fixerDataJson.base,
                                    targetCurrency);
                        }
                    }

                    return ratesState;
                })
                .onErrorReturn(throwable -> {
                    int errorCode = Constants.ERROR_CODE_REQUEST_ERROR;
                    if (throwable instanceof UnknownHostException) {
                        errorCode = Constants.ERROR_CODE_NO_INTERNET_CONNECTION;
                    }

                    return new RatesState(false, errorCode);
                })
                .toObservable()
                .doOnNext(ratesState1 -> FixerDataRepository.this.ratesState = ratesState1);
    }

    public RatesState get() {
        return ratesState;
    }
}
