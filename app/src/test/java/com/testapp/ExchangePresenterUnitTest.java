package com.testapp;

import com.testapp.model.FixerDataInteractor;
import com.testapp.model.entities.RatesState;
import com.testapp.mvp.presenter.ExchangePresenter;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExchangePresenterUnitTest {
    @Mock
    FixerDataInteractor interactor;

    @Test
    public void exchange_isCorrect() throws Exception {
        RatesState ratesState = new RatesState(true, Constants.ERROR_CODE_NONE);
        ratesState.addRate(4.0, "VAL1", "VAL2");

        when(interactor.getRates()).thenReturn(ratesState);

        ExchangePresenter presenter = new ExchangePresenter(interactor);
        presenter.setBaseCurrency("VAL1");
        presenter.setTargetCurrency("VAL2");
        presenter.setTargetValue(34.0);

        Assert.assertEquals(8.5, presenter.createRatesViewModel().baseValue, 0.0);
    }
}